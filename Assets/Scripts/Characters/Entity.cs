using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Entity : MonoBehaviour{

    public Transform myTransform = null;
    public static Transform player = null;
	public Transform enemy = null;
    public Transform playerEnemy = null;
    public bool isAttacking = false;
    public bool isGrounded = false;
    public bool isDead = false;
    
    public float gravity = 140.0f;

    //Entity variables
    public string mobName = null;
    public Transform target = null;
	public int health = 0;
    public int moveSpeed = 0;
    public int rotationSpeed = 0;
    public int strength = 0;
	public int maxHealth = 100;
    public float attackDistance = 0.0f;
    public float viewDistance = 0.0f;
    public float coolDownTime = 0.0f;

    public float attackTime = 0.0f;

	
	
	
    //When called the entity variables are set
    public void InitEntity(string name, Transform targ, int hp, int maxhp, int ms, int rs, int minstr, int maxStr, float ad, float vd, float cdt){
        mobName = name;
        target = targ;
        health = hp;
		maxHealth = maxhp;
        moveSpeed = ms;
        rotationSpeed = rs;
        strength = Random.Range(minstr, maxStr);
        attackDistance = ad;
        viewDistance = vd;
        coolDownTime = cdt;
    }

    void Awake(){
        player = GameObject.Find("Player").transform;
        myTransform = transform;
    }

    void Update(){
        //Entity is destroyed when health is equal or less than 0
        if(health == 0){
            
            Destroy(gameObject);
        }
    }

    void FixedUpdate(){
        //Add gravity
        rigidbody.AddForce(new Vector3 (0, -gravity * rigidbody.mass, 0));
    }

    public void CheckGrounded(){
        RaycastHit hit;
        isGrounded = false;
        Vector3 down = myTransform.TransformDirection(Vector3.down);
        Debug.DrawRay(myTransform.position, down * 1.8f, Color.magenta);

        if(Physics.Raycast(myTransform.position, down, out hit, 1.8f)){
            if(hit.transform.tag == "Floor"){
                isGrounded = true;
            }
        }
    }

	public void AdjustHealth(int adj){
		health -= adj;
		
		if(health<0){
			health=0;
		} 
		if (health > maxHealth){
		    health = maxHealth;
		}
	}
	
	
	/*
    public void DropLoot(){
        Loot loot;
        GameObject lootParent = Instantiate(lootParentPref.gameObject, myTransform.position, Quaternion.identity) as GameObject;
        loot = lootParent.GetComponent<Loot>();
        lootParent.name = myTransform.name + "Loot";
        for(int i = 0; i < lootItemsList.Count; i++){
            GameObject droppedLoot = Instantiate(lootItemsList[i].gameObject, myTransform.position, Quaternion.Euler(-90, Random.Range(0, 360), 0)) as GameObject;
            droppedLoot.transform.parent = lootParent.transform;
            droppedLoot.name = lootItemsList[i].name;
            loot.lootItemsList.Add(droppedLoot.transform);
        }
    }
    */
}
