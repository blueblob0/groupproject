using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Pathfinding;
public class WaypointPath : MonoBehaviour {
    //The point to move to
    public Vector3 targetPosition;
    public List<Transform> waypoints;
    
    public Seeker seeker;
    public CharacterController controller;
 
    public Path path;
    
    public float speed = 1;
    public float nextWaypointDistance = 3;
    public int pathWaypoint = 0;

    public int currentWaypoint = 0;
    public int prevWaypoint = 0;

    public void Start () {
        seeker = GetComponent<Seeker>();
        controller = GetComponent<CharacterController>();
        currentWaypoint = Random.Range(0, waypoints.Count);
        
        seeker.StartPath (transform.position, waypoints[currentWaypoint].position, OnPathComplete);
    }
    
    public void OnPathComplete(Path p){
        if(!p.error){
            path = p;
            pathWaypoint = 0;
        }
    }
 
    public float rotspeed;
    public void Wander(){
        if(path == null){
            return;
        }
        
        if(pathWaypoint >= path.vectorPath.Count){
            prevWaypoint = currentWaypoint;
            do{
                currentWaypoint = Random.Range(0, waypoints.Count);
            }while(currentWaypoint == prevWaypoint);
            pathWaypoint = 0;
            seeker.StartPath (transform.position, waypoints[currentWaypoint].position, OnPathComplete);

            return;
        }
        
        Vector3 dir = (path.vectorPath[pathWaypoint]-transform.position).normalized;
        //dir *= speed * Time.fixedDeltaTime;
        transform.position = Vector3.MoveTowards(transform.position, path.vectorPath[pathWaypoint], speed * Time.deltaTime);
        //controller.SimpleMove(dir);

        Vector3 targetDir = path.vectorPath[pathWaypoint] - transform.position;
        float step = rotspeed * Time.deltaTime;
        Vector3 newDir = Vector3.RotateTowards(transform.forward, targetDir, step, 0.0F);
        transform.rotation = Quaternion.LookRotation(newDir);
        transform.eulerAngles = new Vector3(0,transform.eulerAngles.y,0);
        
        if(Vector3.Distance(transform.position, path.vectorPath[pathWaypoint]) < nextWaypointDistance){
            pathWaypoint++;
            return;
        }
    }

    public void StopPath(){
        path = null;
        pathWaypoint = 0;
        //OnPathComplete(path);
    }
}