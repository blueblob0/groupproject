using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Enemy : Entity{
    public WaypointPath wander;

    public bool isInDistance = false;
    public bool isWandering = false;
    public bool isAggro = false;

	public GameObject LootItemsPref;

	void Start(){
		target = player;
	}
	
	void Update(){
        
        if(health == 0){			
          	DropLoot(); 
           	Destroy(gameObject);
        }
    }
	
	public void Attack(){
        wander = myTransform.GetComponent<WaypointPath>();

        if(Vector3.Distance(myTransform.position, target.position) < viewDistance){
            if(!isAggro){
                isInDistance = true;
                wander.StopPath();
            }
        }else{
            if(!isAggro){
                isInDistance = false;
                wander.Wander();
            }
        }

        RaycastHit hit;
        isAttacking = false;
        Vector3 forward = myTransform.TransformDirection(Vector3.forward);
        Debug.DrawRay(myTransform.position + Vector3.up * 5, forward * attackDistance, Color.green);

        if(Physics.Raycast(myTransform.position + Vector3.up * 5, forward, out hit, attackDistance)){
            if(hit.transform.tag == "Player"){
                isAttacking = true;
            }

            if(myTransform.tag == "Player"){
                playerEnemy = hit.transform;
            }
        }

        if(isAttacking){
            if(attackTime < coolDownTime){
                attackTime += Time.deltaTime;
            }else if(attackTime > coolDownTime){
                attackTime = 0.0f;

                Entity entity = (Entity)target.GetComponent(typeof(Entity));
			    entity.AdjustHealth(strength);
            }
        }
    }

    public void Move(){
        if(isInDistance) transform.position = Vector3.MoveTowards(myTransform.position, target.position, Time.deltaTime * moveSpeed);
    }
	
	public void DropLoot(){
		
		GameObject lootItems = Instantiate(LootItemsPref, myTransform.position,Quaternion.identity)as GameObject;
		lootItems.name = myTransform.name+"loot";
		
		
	}
	 
	
	
	
	
}
