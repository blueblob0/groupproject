using UnityEngine;
using System.Collections;

public class ChargerAI : Enemy{
    public float cooldownChargeTime = 3;
    public float cooldownChargeTimer = 0;
    public bool isCharging;
    public bool hasShunted;

    public Vector3 targetPos;
    GameObject[] floorgos;

    public enum AttackState{
        Idle,
        Walk,
        Charge1,
        Charge2,
        Shunt,
        StunnedTrans,
        Stunned
    }

    public AttackState attackState = AttackState.Walk;

    void Start(){
        InitEntity("Charger", player, 200, 200, 13, 10, 15, 25, 13f, 30, 2);
        cooldownChargeTimer = cooldownChargeTime;
        floorgos = GameObject.FindGameObjectsWithTag("Floor");
    }

	void FixedUpdate(){
        rigidbody.AddForce(new Vector3 (0, -gravity * rigidbody.mass, 0));
        Attack();
        AnimaionManager();
        if(isInDistance || isAggro){
            isAggro = true;
            if(!isAttacking && target){
                if(cooldownChargeTimer <= 0){
                    isCharging = true;
                    attackState = AttackState.Charge2;
                    if(targetPos == Vector3.zero) targetPos = target.position;
                    Charge();
                    isCharging = false;
                }else{
                    myTransform.rotation = Quaternion.Slerp(myTransform.rotation, Quaternion.LookRotation(target.position - myTransform.position), rotationSpeed*Time.deltaTime);
                    cooldownChargeTimer -= Time.deltaTime;
                    if(player.GetComponent<PlayerControls>().stunned){
                        StartCoroutine(Stunned());
                    }else{
                        attackState = AttackState.Charge1;
                    }
                }
            }else{
                cooldownChargeTimer = cooldownChargeTime;
                isCharging = false;
                if(!hasShunted)StartCoroutine(ShuntPlayer());
            }
        }else{
            cooldownChargeTimer = cooldownChargeTime;
        }
	}

    void AnimaionManager(){
        switch(attackState){
            case AttackState.Idle:
                animation.clip = animation.GetClip("idle");
                animation.CrossFade(animation.clip.name, 0.2f);
                break;
            case AttackState.Walk:
                animation.clip = animation.GetClip("walk");
                animation.CrossFade(animation.clip.name, 0.2f);
                break;
            case AttackState.Charge1:
                animation.clip = animation.GetClip("charge1");
                animation.CrossFade(animation.clip.name, 0.2f);
                break;
            case AttackState.Charge2:
                animation.clip = animation.GetClip("charge2");
                animation.CrossFade(animation.clip.name, 0.2f);
                break;
            case AttackState.Shunt:
                animation.clip = animation.GetClip("shunt");
                animation[animation.clip.name].speed = 1.5f;
                animation.CrossFade(animation.clip.name, 0.2f);
                break;
            case AttackState.StunnedTrans:
                animation.clip = animation.GetClip("stunnedTrans");
                animation.CrossFade(animation.clip.name, 0.2f);
                break;
            case AttackState.Stunned:
                animation.clip = animation.GetClip("stunned");
                animation.CrossFade(animation.clip.name, 0.2f);
                break;
        }
    }

    void Charge(){
        //transform.position = Vector3.MoveTowards(myTransform.position, myTransform.forward, Time.deltaTime * 20);
        myTransform.position += myTransform.forward * 20 * Time.deltaTime;
    }

    IEnumerator ShuntPlayer(){
        hasShunted = true;
        attackState = AttackState.Shunt;
        yield return new WaitForSeconds(0.3f);
        rigidbody.velocity = Vector3.zero;
        rigidbody.angularVelocity = Vector3.zero;

        Vector3 targetVelocity = transform.forward;
        //targetVelocity = target.TransformDirection(targetVelocity);
        targetVelocity *= 100;
        target.rigidbody.velocity = targetVelocity;

        StartCoroutine(player.GetComponent<PlayerControls>().Stunned());
        yield return new WaitForSeconds(animation.clip.length / 2);
        hasShunted = false;
        targetPos = Vector3.zero;
    }

    IEnumerator Stunned(){
        attackState = AttackState.Stunned;
        cooldownChargeTimer = cooldownChargeTime;
        isCharging = false;
        
        yield return new WaitForSeconds(animation.clip.length * 4);
        cooldownChargeTimer = cooldownChargeTime;
        targetPos = Vector3.zero;
    }

    void OnCollisionEnter(Collision collision){
        if(collision.transform.tag == "Obstacles"){
            if(isAggro){
                //cooldownChargeTimer = cooldownChargeTime;
                StartCoroutine(Stunned());
            }
        }
    }
}
