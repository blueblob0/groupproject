using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class Items{
	private string name;
	private int cost;
	private RarityNames rarity;
	private int PreWeapCount = 0;
	//private List<Items> lootItems;
		
	
		
	public Items(){
		name = "need name";
		cost= 0;
		rarity = RarityNames.Common;
		
		}
	
	
	public Items(string itemName, int itemCost,RarityNames rare){
		name = itemName;
		cost= itemCost;
		rarity = rare;
		
		}
		
	
	
	
	public string Name{
	get { return name;}
		set{name = value;}
	}
	
	public int Cost{
	get { return cost;}
		set{cost = value;}
	}
			
	public RarityNames Rarity {
	get { return  rarity;}
		set{rarity = value;}
	}		
			
			
			
			
	// Use this for initialization
	void Start () {
		
	
		
	
		
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	
	
	
	
	
}

public enum RarityNames{
	
	Common,
	Mystic,
	Chest
	
	}


	