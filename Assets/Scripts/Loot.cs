using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Loot : MonoBehaviour{
	
	private static List<Items> lootItemNames = new List<Items>();
	private string lootDescription= "";
	private int lootnumber=0;	
	private int droppedloot = 1;
	private string lootName;
	private int oldLootNameLength=0;
	private string EnemyType="";
	private Vector3 position;
	private string[] WeaponPreName;	
	
   
	public static List<Items> LootItemNames{
		get{return lootItemNames; }
	}

	void Start(){
		AddPreWeponNames();
		Assignloot();
	}
	
	void Update(){
		
	    //if(transform.childCount <= 0) Destroy(gameObject);
	}
	
	
	public void OnMouseEnter(){
		
		Messenger<List<Items>>.Broadcast("ShowLoot",LootItemNames);
		
		}
	
	public void OnMouseExit(){
		
		Messenger.Broadcast ("HideLoot");
		
		}
	
	public void OnMouseUp(){
		
		Messenger.Broadcast ("AddLootToBag");
		}
	
	private void Assignloot(){
		for(int i=0;i<droppedloot;i++){	
			
			lootnumber=(Random.Range(0,11));
			lootDescription= WeaponPreName[lootnumber] ;  //   Random.Range(1,14)
			lootItemNames.Add (new Items());
			lootName=(lootDescription + " Staff");
			lootItemNames[i].Name =lootName; //change so each number can represent a diffrent name
			lootItemNames[i].Cost=(Random.Range(50,100));
			

		}
	}
	
	private void AddPreWeponNames(){
		WeaponPreName = new string[12];
		WeaponPreName[0]=("Pointy");
		WeaponPreName[1]=("Oak");
		WeaponPreName[2]=("Iron");
		WeaponPreName[3]=("Burnt");
		WeaponPreName[4]=("Shiny");
		WeaponPreName[5]=("Wonky");
		WeaponPreName[6]=("Straight");
		WeaponPreName[7]=("Thin");
		WeaponPreName[8]=("Sticky");
		WeaponPreName[9]=("Steel");//10
		WeaponPreName[10]=("Hard"); 
		WeaponPreName[11]=("Soft");
		
		
	}
}
