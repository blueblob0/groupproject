using UnityEngine;
using System.Collections;

public class Spells : MonoBehaviour{

    Enemy enemy;
    public int spellStrength = 0;
    
    public void Init(int ss){
        spellStrength = ss;
    }

    void OnCollisionEnter(Collision collision){
        if(collision.transform.tag == "Enemy"){
            Entity entity = (Entity)collision.transform.GetComponent(typeof(Entity));
			entity.AdjustHealth(spellStrength);
        }
    }
}
