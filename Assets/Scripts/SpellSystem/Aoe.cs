using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Aoe : MonoBehaviour{

	private Transform myTransform;

    public List<Transform> components;
    public float scrollSpeed = 0.5F;

    bool place;

	void Start(){
		myTransform = transform;
	}
 
	void Update(){
        components[0].Rotate(0, 0, 0.5f);
        components[0].GetComponent<Projector>().fieldOfView+=0.01f;
        components[1].Rotate(0, 0, -0.5f);
        components[1].GetComponent<Projector>().fieldOfView+=0.01f;

        components[2].Rotate(0, 0, 0.5f);

        float offset = Time.time * scrollSpeed;
        components[3].renderer.material.SetTextureOffset("_MainTex", new Vector2(offset, 0));
    }
}